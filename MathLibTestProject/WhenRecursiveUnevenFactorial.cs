﻿using FluentAssertions;
using Xunit;

namespace MathLib.Tests.Unit
{
    public class WhenRecursiveUnevenFactorial
    {
        [Theory]
        [InlineData(0, 1)]
        [InlineData(1, 1 * 1)]
        [InlineData(2, 1 * 1)]
        [InlineData(3, 3 * 1)]
        [InlineData(4, 3 * 1)]
        [InlineData(5, 5 * 3 * 1)]
        public void ThenResultsShouldMatch(int input, int expectation)
        {
            var ml = new MathLib(ModeToCalculate.Recursive);
            ml.CalcUnevenFactorial(input).Should().Be(expectation);
        }
    }
}
