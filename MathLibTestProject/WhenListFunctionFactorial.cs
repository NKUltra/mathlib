﻿using FluentAssertions;
using Xunit;

namespace MathLib.Tests.Unit
{
    public class WhenListFunctionFactorial
    {
        [Theory]
        [InlineData(0, 1)]
        [InlineData(1, 1 * 1)]
        [InlineData(2, 2 * 1)]
        [InlineData(3, 3 * 2 * 1)]
        [InlineData(4, 4 * 3 * 2 * 1)]
        [InlineData(5, 5 * 4 * 3 * 2 * 1)]
        public void ThenResultsShouldMatch(int input, int expectation)
        {
            var ml =  new MathLib(ModeToCalculate.ListFunction);
            ml.CalcFactorial(input).Should().Be(expectation);            
        }
    }
}
