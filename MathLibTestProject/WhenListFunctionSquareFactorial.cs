﻿using FluentAssertions;
using Xunit;

namespace MathLib.Tests.Unit
{
    public class WhenListFunctionSquareFactorial
    {
        [Theory]
        [InlineData(0, 1)]
        [InlineData(1, 1 * 1)]
        [InlineData(2, 2 * 2)]
        [InlineData(3, 3 * 3 * 2 * 2)]
        [InlineData(4, 4 * 4 * 3 * 3 * 2 * 2 * 1 * 1)]
        [InlineData(5, 5 * 5 * 4 * 4 * 3 * 3 * 2 * 2 * 1 * 1)]
        public void ThenResultsShouldMatch(int input, int expectation)
        {
            var ml = new MathLib(ModeToCalculate.ListFunction);
            ml.CalcSquareFactorial(input).Should().Be(expectation);
        }
    }
}
