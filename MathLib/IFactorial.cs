﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MathLib
{
    interface IFactorial
    {
        int Calc(int n);
        int CalcUneven(int n);
        int CalcSquare(int n);
    }
}
