﻿using System;

namespace MathLib
{
    public sealed class MathLib
    {
        private readonly IFactorial fac;

        public MathLib(ModeToCalculate mtc)
        {
            if (mtc == ModeToCalculate.ListFunction)
            {
                this.fac = new FactorialListFunction();
            }

            if (mtc == ModeToCalculate.Recursive)
            {
                this.fac = new FactorialRecursiv();
            }
        }

        public int CalcFactorial(int n)
        {
            return fac.Calc(n);
        }

        public int CalcUnevenFactorial(int n)
        {
            return fac.CalcUneven(n);
        }

        public int CalcSquareFactorial(int n)
        {        
            return fac.CalcSquare(n);
        }       
    }
}
