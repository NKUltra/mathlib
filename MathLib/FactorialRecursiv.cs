﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathLib
{
    class FactorialRecursiv : IFactorial
    {
        public int Calc(int n)
        {
            return _Calc(n, FactorialOptions.all);
        }

        public int CalcUneven(int n)
        {
            return _Calc(n, FactorialOptions.uneven);
        }

        public int CalcSquare(int n)
        {
            return Convert.ToInt32(Math.Pow(Convert.ToDouble(_Calc(n, FactorialOptions.all)),2));
        }

        private int _Calc(int n, FactorialOptions opt)
        {
            if (n == 0 || n == 1)
            {
                return 1;
            }
            if (opt != FactorialOptions.all)
            {
                if ((opt == FactorialOptions.even && n % 2 != 0) ||
                   (opt == FactorialOptions.uneven && n % 2 == 0))
                {
                    return _Calc(n - 1, opt);
                }                
            }

            return n * _Calc(n - 1,opt);

        }
    }

}
