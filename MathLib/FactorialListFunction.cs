﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathLib
{
    class FactorialListFunction : IFactorial
    {
        public int Calc(int n)
        {
            return _Calc(n, FactorialOptions.all);
        }

        public int CalcUneven(int n)
        {
            return _Calc(n, FactorialOptions.uneven);
        }

        public int CalcSquare(int n)
        {
            return Convert.ToInt32(Math.Pow(Convert.ToDouble(_Calc(n, FactorialOptions.all)), 2));
        }

        private int _Calc(int n, FactorialOptions opt)
        {
            return  Enumerable.Range(1, n).Where(
                                                    x =>
                                                       opt == FactorialOptions.all 
                                                    || opt == FactorialOptions.uneven && x%2!=0
                                                    || opt == FactorialOptions.even && x%2 == 0
                                                  ).Aggregate(1,(i, j) => i * j);

        }
    }
}
